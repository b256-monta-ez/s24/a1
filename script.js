// console.log("Hello World");

let getCube = 5 ** 5;
console.log(`The cube of 5 is ${getCube}`);

let address = ["1600", "Pennsylvania Avenue", "NW", "Washington", "DC", "20500"];

let [add1, add2, add3, add4, add5, add6] = address;
console.log(`I live at ${add1} ${add2} ${add3} ${add4},${add5} ${add6}.`);

animal = {

	name: "Chris P. Bacon",
	weight: 350,
	age: 9,
	type: "Pig"

}

let animalSentence = `${animal.name} is a one pot-bellied who share his own problems with only having 2 legs. The ${animal.age} year old ${animal.type} weighs ${animal.weight} kgs.`
console.log(animalSentence);


let numbersArr = [1, 3, 5, 7];

numbersArr.forEach((numbersArr) => console.log(numbersArr));

let reducedNumber = numbersArr.reduce((x, y) => {
 return x + y;
});
console.log(reducedNumber);

class dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dog1 = new dog("Hairy Pawter", 4, "Pug");
console.log(dog1);
let dog2 = new dog("J.K. Growling", 10, "St. Bernard");
console.log(dog2);
let dog3 = new dog("Katy Pawry", 3, "Pomeranian");
console.log(dog3);